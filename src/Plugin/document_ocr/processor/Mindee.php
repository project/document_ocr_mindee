<?php

namespace Drupal\document_ocr_mindee\Plugin\document_ocr\processor;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\TempStore\PrivateTempStoreFactory;
use Drupal\Core\File\FileSystemInterface;
use Drupal\Core\Logger\LoggerChannelFactory;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\document_ocr\Plugin\ProcessorBase;
use Drupal\document_ocr_mindee\Mindee as MindeeService;

/**
 * Mindee processor plugin.
 *
 * @DocumentOcrProcessor(
 *   id = "mindee",
 *   name = @Translation("Mindee"),
 *   extensions = "pdf heic tiff tif jpg jpeg png webp",
 *   requires = {
 *     "credentials",
 *     "template"
 *   },
 *   supports = {
 *     "store_json",
 *   }
 * )
 */
class Mindee extends ProcessorBase {

  /**
   * Mindee serivce.
   *
   * @var \Drupal\document_ocr_mindee\Mindee as MindeeService
   */
  protected $client;

  /**
   * {@inheritdoc}
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, PrivateTempStoreFactory $temp_store_factory, FileSystemInterface $file_system, LoggerChannelFactory $logger_factory, MindeeService $mindee) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $temp_store_factory, $file_system, $logger_factory);
    $this->loggerFactory = $logger_factory->get('document_ocr_mindee');
    $this->client = $mindee;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static($configuration, $plugin_id, $plugin_definition,
      $container->get('tempstore.private'),
      $container->get('file_system'),
      $container->get('logger.factory'),
      $container->get('document_ocr_mindee.client')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function requirementsAreMet() {
    return TRUE;
  }

  /**
   * {@inheritdoc}
   */
  public function configurationForm(array $form, FormStateInterface $form_state) {
    $apis = $this->client->getApis();
    $options = [];
    foreach ($apis as $key => $details) {
      $options[$key] = $details['name'];
    }
    $form['api'] = [
      '#type' => 'select',
      '#title' => $this->t('API'),
      '#options' => $options,
      '#empty_option' => $this->t('- Select API -'),
      '#default_value' => !empty($this->configuration['api']) ? $this->configuration['api'] : '',
      '#required' => TRUE,
      '#disabled' => !empty($this->configuration['api']),
      '#ajax' => [
        'callback' => [$this, 'ajaxApiVersionForm'],
        'wrapper' => 'version-container',
        'method' => 'replace',
      ],
    ];

    if (!empty($this->configuration)) {
      $version_list = !empty($apis[$this->configuration['api']]['versions']) ? $apis[$this->configuration['api']]['versions'] : [];
      $form['version'] = [
        '#title' => $this->t('Version'),
        '#type' => 'select',
        '#empty_option' => $this->t('- Select Version -'),
        '#options' => array_combine($version_list, $version_list),
        '#description' => $this->t('Select API version'),
        '#default_value' => !empty($this->configuration['version']) ? $this->configuration['version'] : '',
        '#required' => TRUE,
      ];
    }
    else {
      $form['version_container'] = [
        '#type' => 'container',
        '#prefix' => '<div id="version-container">',
        '#suffix' => '</div>',
      ];
      if (!empty($form_state->getValue('api'))) {
        $versions = [];
        $version_list = !empty($apis[$form_state->getValue('api')]['versions']) ? $apis[$form_state->getValue('api')]['versions'] : [];
        foreach ($version_list as $version) {
          $versions[$version] = $version;
        }
        $form['version_container']['version'] = [
          '#title' => $this->t('Version'),
          '#type' => 'select',
          '#empty_option' => $this->t('- Select Version -'),
          '#options' => array_combine($versions, $versions),
          '#description' => $this->t('Select API version'),
          '#default_value' => !empty($this->configuration['version']) ? $this->configuration['version'] : '',
          '#required' => TRUE,
        ];
      }
    }
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function configurationValues(array &$form, FormStateInterface $form_state) {
    if (!empty($this->configuration)) {
      return [
        'api' => $this->configuration['api'],
        'version' => $form_state->getValue('version'),
      ];
    }
    else {
      return [
        'api' => $form_state->getValue('api'),
        'version' => $form_state->getUserInput()['version'],
      ];
    }
  }

  /**
   * {@inheritdoc}
   */
  public function getMappingData() {
    try {
      $document = $this->client->setCredentials($this->getCredentials())
        ->setEndpoint($this->configuration)
        ->processDocument($this->getFile());
      if (!empty($document['document']['inference']['prediction'])) {
        $text = [];
        foreach ($document['document']['inference']['prediction'] as $prediction => $params) {
          $value = !empty($params['value']) ? $params['value'] : '';
          $this->setOption($prediction, $prediction);
          $this->setOptionValue($prediction, $value);
          if (!empty($value)) {
            $text[] = $prediction . ': ' . $value;
          }
        }
        if (!empty($text)) {
          $this->setOption('extracted_text', $this->t('Extracted Text'));
          $this->setOptionValue('extracted_text', join("\r\n", $text));
        }
        $this->setDocument($document['document']);
      }
      return $this;
    }
    catch (\Exception $ex) {
      $this->loggerFactory->error($ex->getMessage());
      return FALSE;
    }
  }

  /**
   * Handles switching API version selector.
   */
  public function ajaxApiVersionForm($form, FormStateInterface $form_state) {
    return $form['version_container'];
  }

}
