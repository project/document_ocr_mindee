<?php

namespace Drupal\document_ocr_mindee;

use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\File\FileSystemInterface;
use GuzzleHttp\Client;

/**
 * Mindee service.
 */
class Mindee {

  use StringTranslationTrait;

  /**
   * The file system service.
   *
   * @var \Drupal\Core\File\FileSystemInterface
   */
  protected $fileSystem;

  /**
   * HTTP client.
   *
   * @var GuzzleHttp\Client
   */
  protected $httpClient;

  /**
   * API Key.
   */
  protected $credentials = [];

  /**
   * API Endpoint.
   */
  protected $api_endpoint;

  /**
   * {@inheritdoc}
   */
  public function __construct(FileSystemInterface $file_system, Client $http_client) {
    $this->fileSystem = $file_system;
    $this->httpClient = $http_client;
  }

  /**
  * Supported APIs.
  */
  public function getApis() {
    return \Drupal::service('document_ocr_mindee.apis_repository')->toArray();
  }

  /**
   * Set JSON credentials file.
   */
  public function setCredentials($credentials) {
    $this->credentials = $credentials;
    return $this;
  }

  /**
   * Creates a completion for the provided prompt and parameters.
   */
  public function setEndpoint($configuration) {
    $this->api_endpoint = 'https://api.mindee.net/v1/products/mindee/' . $configuration['api'] . '/' . $configuration['version'] . '/predict';
    return $this;
  }

  /**
   * Process document.
   */
  public function processDocument($file) {
    try {
      $documentPath = $this->fileSystem->realpath($file->getFileUri());
      $handle = fopen($documentPath, 'rb');
      $contents = fread($handle, filesize($documentPath));
      fclose($handle);
      $response = $this->httpClient->post($this->api_endpoint, [
        'headers' => [
          'Authorization' => 'Token ' . $this->credentials['apikey'],
          'Accept' => 'application/json',
        ],
        'form_params' => [
          'document' => base64_encode($contents),
        ]
      ]);
      return json_decode($response->getBody()->getContents(), true);
    }
    catch (\Exception $ex) {
      \Drupal::logger('document_ocr_mindee')->error($ex->getMessage());
    }
  }

}
