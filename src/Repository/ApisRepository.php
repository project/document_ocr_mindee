<?php

namespace Drupal\document_ocr_mindee\Repository;

use Drupal\document_ocr\Repository\JsonRepositoryBase;

/**
 * Mindee APIs respository class.
 *
 * @package document_ocr_mindee
 */
class ApisRepository extends JsonRepositoryBase {
  protected $module = 'document_ocr_mindee';
  protected $repository = 'apis';
}
