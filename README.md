# Document OCR Mindee

Document OCR integration with Mindee. Mindee - Accurate and lightning-fast document parsing API.

For a full description of the module, visit the
[project page](https://www.drupal.org/project/document_ocr_mindee).

Submit bug reports and feature suggestions, or track changes in the
[issue queue](https://www.drupal.org/project/issues/document_ocr_mindee).

## Table of contents

- Requirements
- Installation
- Configuration
- Maintainers

## Requirements

This module requires the Document OCR module (https://www.drupal.org/project/document_ocr)

## Installation

Install as you would normally install a contributed Drupal module. For further
information, see
[Installing Drupal Modules](https://www.drupal.org/docs/extending-drupal/installing-drupal-modules).

## Configuration

Configure the Document OCR Mindee at (/admin/config/structure/document-ocr).

### [Mindee](https://mindee.com/)

1. Create API Key [here](https://platform.mindee.com/api-keys)
2. Get API key and create JSON file and store your credentials in the following format:
```
{
  "apikey": "[API-KEY]"
}
```
4. Upload it to the private directory `private://document-ocr/mindee-credentials.json`
5. Point to the credentials file when adding Mindee processor
6. You should be all set to use Mindee integration

## Maintainers

- [Minnur Yunusov (minnur)](https://www.drupal.org/u/minnur)
